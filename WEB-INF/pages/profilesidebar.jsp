<%@ include file="/common/taglibs.jsp" %>
<div class="sidebar">
    <div class="mini-submenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </div>
    <div class="list-group">
       <div class="row">
        <a href="myideas" class="col-sm-6 list-group-item2">
            <i class="fa fa-user-md"></i> My Ideas
        </a>
        <a href="recent" class="col-sm-6 list-group-item2">
            <i class="fa fa-clock-o"></i> My Mentors
        </a>
        </div>
        <div class="row">
        <a href="trending" class="col-sm-6 list-group-item2">
            <i class="fa fa-line-chart"></i> Investors
        </a>
        <a href="ideaform" class="col-sm-6 list-group-item2">
            <i class="fa fa-plus-circle"></i> Add an
            Idea
        </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="collapse"]').on('click', function () {
            var $this = $(this),
                    $parent = typeof $this.data('parent') !== 'undefined' ? $($this.data('parent')) : undefined;
            if ($parent === undefined) { /* Just toggle my  */
                $this.find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
                return true;
            }

            /* Open element will be close if parent !== undefined */
            var currentIcon = $this.find('.glyphicon');
            currentIcon.toggleClass('glyphicon-plus glyphicon-minus');
            $parent.find('.glyphicon').not(currentIcon).removeClass('glyphicon-minus').addClass('glyphicon-plus');

        });
        $('#slide-submenu').on('click', function () {
            $(this).closest('.list-group').fadeOut('slide', function () {
                $('.mini-submenu').fadeIn();
            });

        });

        $('.mini-submenu').on('click', function () {
            $(this).next('.list-group').toggle('slide');
            $('.mini-submenu').hide();
        })
    });
</script>
