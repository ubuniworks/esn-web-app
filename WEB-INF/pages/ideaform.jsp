<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="ideaDetail.title"/></title>
    <meta name="menu" content="IdeaMenu"/>
    <meta name="heading" content="<fmt:message key='ideaDetail.heading'/>"/>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'.textarea' });</script>
    <link href="assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />
     <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
</head>
      <body class="page-container-bg-solid">
 <div class="page-container">
        <div class="page-content-wrapper">
            <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add an Idea
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->
                            <div class="btn-group btn-theme-panel">
                                <a class="btn dropdown-toggle" href="<c:url value='logout'/>">
                                    Click here to Logout
                                    <i class="icon-power"></i>
                                </a>
                              
                            </div>
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
            <div class="page-content">
        <div class="container">
            <div class="page-content-inner">
                <div class="inbox">
<c:set var="delObject" scope="request"><fmt:message key="ideaList.idea"/></c:set>
<script type="text/javascript">var msgDelConfirm =
        "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<div class="row">
<div class="col-md-3">
    <%@include file="ideasidebar.jsp" %>
</div>

<div class="col-md-9">
     <div class="todo-content">
            <div class="portlet light ">
        
    <h2><fmt:message key="ideaDetail.heading"/></h2>
    <fmt:message key="ideaDetail.message"/>
    <form:errors path="*" cssClass="alert alert-danger alert-dismissable" element="div"/>
    <form:form commandName="idea" method="post" action="ideaform" cssClass="form-bordered"
               id="ideaForm" onsubmit="return validateIdea(this)">
        <form:hidden path="ididea"/>
        <form:hidden path="datecreated"/>
        <spring:bind path="idea.title">
            <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
        <appfuse:label key="idea.title" styleClass="control-label"/>
        <form:input cssClass="form-control" path="title" id="title" maxlength="45"/>
        <form:errors path="title" cssClass="help-block"/>
        </div>
        <spring:bind path="idea.description">
            <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
        <appfuse:label key="idea.description" styleClass="control-label"/>
        <form:input cssClass="form-control" path="description" id="description" maxlength="255"/>
        <form:errors path="description" cssClass="help-block"/>
        </div>
        <form:hidden path="ideabody.idideabody"/>
        <spring:bind path="ideabody.content">
            <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
        <appfuse:label key="ideabody.content" styleClass="control-label"/>
        <form:textarea class="textarea form-control" path="ideabody.content" id="content" rows="6"/>
        <form:errors path="ideabody.content" cssClass="help-block"/>
        </div>
        <!-- todo: change this to read the identifier field from the other pojo -->
        <%--<form:select cssClass="form-control" path="user" items="userList" itemLabel="label" itemValue="value"/>--%>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" id="save" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>
            <c:if test="${not empty idea.ididea}">
                <button type="submit" class="btn btn-danger" id="delete" name="delete"
                        onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                    <i class="icon-trash icon-white"></i> <fmt:message key="button.delete"/>
                </button>
            </c:if>

            <button type="submit" class="btn btn-default" id="cancel" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        
        </div> </div> </div>
    </form:form>


<v:javascript formName="idea" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value='/scripts/validator.jsp'/>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("input[type='text']:visible:enabled:first", document.forms['ideaForm']).focus();
    });
</script>
</div>
    </div>
    </div>
</div>
</div>
                </div>
            </div>
</div>
          <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
    </body>