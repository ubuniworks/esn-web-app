<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="home.title"/></title>
    <meta name="menu" content="Home"/>
</head>
<body class="page-container-bg-solid">
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Dashboard
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                         <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->
                            <div class="btn-group btn-theme-panel">
                                     <a class="btn dropdown-toggle" href="<c:url value='/home'/>"><i class="icon-calendar icons"></i> Back to Dashboard</a> |
                                <a class="btn dropdown-toggle" href="<c:url value='logout'/>">
                                    Click here to Logout
                                    <i class="icon-power"></i>
                                </a>
                              
                            </div>
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
<div class="page-content">
    <div class="container">
<!-- Page Features -->
<div class="row major">
        <div class="col-sm-6">
            <a href="<c:url value='/profile'/>">
              <div class="icon-menu text-center">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <div class="icon-box-area"> 
                            <h3>My Profile</h3></div>
                       
              
            </a>
        </div>
        <div class="col-sm-6">
          <a href="<c:url value='/ideas'/>">
               
                 <div class="icon-menu text-center">
                     <span class="icon-ideas"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>   </div>
                        <div class="icon-box-area"> 
                            <h3>My Ideas</h3></div>
                       
                
            </a>
        </div>
        </div>
        <div class="row major">
        
        <div class="col-sm-6">
           <a href="#">
          
                 <div class="icon-menu text-center">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                        <div class="icon-box-area"> 
                            <h3>My Investors/ Mentors</h3></div>
               
            </a>
        </div>    
      
        
        <div class="col-sm-6">
            <a href="#">
             
                 <div class="col-sm-4 icon-menu text-center">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        </div>
                        <div class="col-sm-8 icon-box-area"> 
                            <h3>My Hub</h3></div>
                       
               
            </a>
        </div>    
       
    </div>
        
</div>
    </div>
</div>
    </div>
  
</body>
