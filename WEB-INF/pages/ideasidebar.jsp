<%@ include file="/common/taglibs.jsp" %>
<div class="inbox-sidebar">
    <a href="ideaform" class="btn green compose-btn btn-block">
     Add an Idea <i class="icon-plus icons"></i></a>
    <ul class="inbox-nav">
        
        <li class="active"><a href="ideas">
            <i class="fa fa-list"></i> All Ideas
        </a></li>
        <li><a href="myideas">
            <i class="fa fa-user-md"></i> My Ideas
        </a></li>
        <li><a href="recent">
            <i class="fa fa-clock-o"></i> Recent
        </a></li>
       <li> <a href="trending">
            <i class="fa fa-line-chart"></i> Trending
        </a></li>
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="collapse"]').on('click', function () {
            var $this = $(this),
                    $parent = typeof $this.data('parent') !== 'undefined' ? $($this.data('parent')) : undefined;
            if ($parent === undefined) { /* Just toggle my  */
                $this.find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
                return true;
            }

            /* Open element will be close if parent !== undefined */
            var currentIcon = $this.find('.glyphicon');
            currentIcon.toggleClass('glyphicon-plus glyphicon-minus');
            $parent.find('.glyphicon').not(currentIcon).removeClass('glyphicon-minus').addClass('glyphicon-plus');

        });
        $('#slide-submenu').on('click', function () {
            $(this).closest('.list-group').fadeOut('slide', function () {
                $('.mini-submenu').fadeIn();
            });

        });

        $('.mini-submenu').on('click', function () {
            $(this).next('.list-group').toggle('slide');
            $('.mini-submenu').hide();
        })
    });
</script>
