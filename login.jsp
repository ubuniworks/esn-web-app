<%@ include file="/common/taglibs.jsp" %>
 
<head>
    <title><fmt:message key="login.title"/></title>
  <meta name="decorator" content="login">
  
</head>
<body id="login" class="page-container-bg-solid">
 <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                     <!-- BEGIN CONTENT BODY-->
             <section class="login-container">
            <div class="container">
                <div class="row">
                <div class="col-sm-6">
                    <div class="icon-big"></div>
                    </div>
                    <div class="col-sm-6">
                        <h4 class="title-small">Be Part of the Great</h4>
                        <h3 class="title-medium">Enterpreneurs Support Network</h3>
                        <div class="form-signup">
                       
<form method="post" id="loginForm" action="<c:url value='/j_security_check'/>"
      onsubmit="saveUsername(this);return validateForm(this)" class="form-horizontal signup-form" autocomplete="off" role="form">
    <h2 class="form-signin-heading">
        <fmt:message key="login.heading"/>
    </h2>
    <c:if test="${param.error != null}">
        <div class="alert alert-danger alert-dismissable">
            <fmt:message key="errors.password.mismatch"/>
        </div>
    </c:if>
    <div class="col-sm-12">
    <div class="form-group" style="
    margin-bottom: 16px;
">
                                                    <label class="sr-only" for="exampleInputEmail22">Email address</label>
                                                    <div class="input-icon">
                                                       
    <input type="text" name="j_username" id="j_username" class="form-control"
           placeholder="<fmt:message key="label.username"/>" required tabindex="1">
                                                        </div>
                                                </div>
    </div>
     <div class="col-sm-12">
    <div class="form-group">
                                                    <label class="sr-only" for="exampleInputPassword42">Password</label>
                                                    <div class="input-icon">
                                                       
    <input type="password" class="form-control" name="j_password" id="j_password" tabindex="2"
           placeholder="<fmt:message key="label.password"/>" required>
                                                        </div>
                                                </div>
    </div>
   <div class="col-sm-12">
     <div class="form-group">
        <div class="mt-checkbox-list"> 
            <c:if test="${appConfig['rememberMeEnabled']}">
        <label for="rememberMe" class="mt-checkbox mt-checkbox-outline">
            <input type="checkbox" name="_spring_security_remember_me" id="rememberMe" tabindex="3"/>
            <span></span>
            <fmt:message key="login.rememberMe"/></label>
    </c:if>
</div>
    </div>
    </div>
   
    <button type="submit" class="btn btn-login" name="login" tabindex="4">
        <fmt:message key='button.login'/>
    </button>


<div class="row">
<div class="col-sm-12"><fmt:message key="login.passwordHint"/></div>
    <div class="col-sm-12">
<fmt:message key="updatePassword.requestRecoveryTokenLink"/>    
</div>
</div>

</form>                  
                             
                             <div class="row hr-line">
                             <hr class="style2">
                                 <span class="or">OR</span>
                                
                             </div>
                             <div class="row">
                             <div class="col-sm-12">
                                 <h4 class="small-title-odd">Login with</h4>  
                                 <a href="#" class=" social-logins-icons facebook col-sm-4">Facebook</a>
                                <a href="#" class="social-logins-icons google col-sm-4">Google</a>
                                <a href="#" class="social-logins-icons linkedin col-sm-4">Linkedin</a>
                                </div>
                             </div>
                             <div class="row">
                             <div class="col-sm-12">  <h4 class="small-title">Join now</h4></div>
                             <div class="col-sm-12">
                                   <fmt:message key="login.signup">
       <p> <fmt:param><c:url value="/signup"/></fmt:param>
    </fmt:message>

<c:set var="scripts" scope="request">
    <%@ include file="/scripts/login.js" %>
</c:set>


                                 </div>
                             
                             </div>
                                          
                        </div>
                    </div>
                </div>
                 </div>
            </section>
        <!-- END CONTAINER -->
            </div>
    </div>
  
</body>