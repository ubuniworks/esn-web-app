<!DOCTYPE html>
<%@ include file="/common/taglibs.jsp" %>
    <html lang="en">

    <head>
        <meta http-equiv="Cache-Control" content="no-store" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="<c:url value=" /images/logo-sm.png "/>"/>
        <title>
            <decorator:title/> |
            <fmt:message key="webapp.name" />
        </title>
        <t:assets type="css" />
        <link href="/styles/font-awesome-4.6.2/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        <link href="/styles/sidemenu.css" rel='stylesheet' type='text/css'>
        <%--<link href="/styles/bootswatch-paper.css" rel='stylesheet' type='text/css'>--%>
            <decorator:head/>
            <t:assets type="js" />
            <%= (request.getAttribute("scripts") != null) ? request.getAttribute("scripts") : "" %>
    </head>
    <body<decorator:getProperty property="body.id" writeEntireProperty="true" />
    <decorator:getProperty property="body.class" writeEntireProperty="true" />>
    <c:set var="currentMenu" scope="request">
        <decorator:getProperty property="meta.menu" />
    </c:set>
    <!-- BEGIN HEADER -->
    <div class="page-header">
        <!-- BEGIN HEADER TOP -->
        <div class="page-header-top">
            <div class="container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<c:url value='/'/>"><img class="logo-default" alt="<fmt:message key="webapp.name"/>" src="assets/layouts/layout3/img/logo.png"></a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->

                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP LOGIN AREA -->
                <div class="login-area">

                    <form class="form-inline login-form" role="form">
                        <div class="col-sm-12">
                            <div class="form-group" style="
    margin-bottom: 16px;
">
                                <label class="sr-only" for="exampleInputEmail22">Email address</label>
                                <div class="input-icon">

                                    <input type="email" class="form-control input-medium" id="exampleInputEmail22" placeholder="Enter Email or Phone Number..."> </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword42">Password</label>
                                <div class="input-icon">

                                    <input type="password" class="form-control input-medium" id="exampleInputPassword42" placeholder="Enter Password here..."> </div>
                                <a href="#" class="forgot-pass">Forgot Password?</a>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-login">Login</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- END HEADER TOP -->

    </div>

    <div class="container" id="content">
        <%@ include file="/common/messages.jsp" %>
            <div class="row">
                <decorator:body/>

                <c:if test="${currentMenu == 'AdminMenu'}">
                    <div class="col-sm-2">
                        <menu:useMenuDisplayer name="Velocity" config="navlistMenu.vm" permissions="rolesAdapter">
                            <menu:displayMenu name="AdminMenu" />
                        </menu:useMenuDisplayer>
                    </div>
                </c:if>
            </div>
    </div>

    <div id="footer" class="container navbar-fixed-bottom">
        <span class="col-sm-6 text-left"><fmt:message key="webapp.version"/>
            <c:if test="${pageContext.request.remoteUser != null}">
                | <fmt:message key="user.status"/> ${pageContext.request.remoteUser}
            </c:if>
        </span>
        <span class="col-sm-6 text-right">
            &copy; <fmt:message key="copyright.year"/> <a href="<fmt:message key="company.url"/>"><fmt:message
                key="company.name"/></a>
        </span>
    </div>
    <script>
        $.each($('*'), function () {
            if ($(this).width() > $('body').width()) {
                console.log("Wide Element: ", $(this), "Width: ", $(this).width());
            }
        });
    </script>
    </body>

    </html>