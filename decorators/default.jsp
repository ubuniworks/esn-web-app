<!DOCTYPE html>
<%@ include file="/common/taglibs.jsp" %>
<html lang="en">
<head>
    <meta http-equiv="Cache-Control" content="no-store"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<c:url value="/images/logo-sm.png"/>"/>
    <title><decorator:title/> | <fmt:message key="webapp.name"/></title>
    <t:assets type="css"/>
    <link href="<c:url value="styles/font-awesome-4.6.2/css/font-awesome.min.css"/>" rel='stylesheet' type='text/css'>
    <link href="<c:url value="styles/sidemenu.css"/>" rel='stylesheet' type='text/css'>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<c:url value="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"/>" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/global/plugins/morris/morris.css"/>" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/global/plugins/fullcalendar/fullcalendar.min.css"/>" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/global/plugins/jqvmap/jqvmap/jqvmap.css"/>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<c:url value="assets/global/css/components.min.css"/>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<c:url value="assets/global/css/plugins.min.css"/>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<c:url value="assets/layouts/layout3/css/layout.min.css"/>" rel="stylesheet" type="text/css" />
        <link href="<c:url value="assets/layouts/layout3/css/themes/default.min.css"/>" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<c:url value="assets/layouts/layout3/css/custom.min.css"/>" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    
    <decorator:head/>
    <t:assets type="js"/>
    <%= (request.getAttribute("scripts") != null) ? request.getAttribute("scripts") : "" %>
</head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true"/><decorator:getProperty property="body.class"
                                                                                                  writeEntireProperty="true"/>>
<c:set var="currentMenu" scope="request"><decorator:getProperty property="meta.menu"/></c:set>

 <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<c:url value='/'/>">
                            <img src="<c:url value="assets/layouts/layout3/img/logo.png"/>" alt="<fmt:message key="webapp.name"/>" class="logo-default">
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP LOGIN AREA -->
                   <div class="top-menu">
 <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default">7</span>
                                </a>
                            
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-calendar"></i>
                                    <span class="badge badge-default">3</span>
                                </a>
                               
                            </li>
                            <!-- END TODO DROPDOWN -->
                            <li class="droddown dropdown-separator">
                                <span class="separator"></span>
                            </li>
                            <!-- END INBOX DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-light">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                                 <span class="username username-hide-mobile">Welcome ${user.fullName}</span>
                                </a>
                                <ul class="dropdown-menu extended tasks">
                                    <li class="external">
                                       <a href="<c:url value="/profile-overview"/>"><i class="icon-user"></i> Profile Overview</a>
                                    </li>
                                    <li class="external">
                                       <a href="<c:url value="/profile"/>"><i class="icon-settings"></i> Account Setting</a>
                            </li>
                                    <li class="external">
                                       <a href="<c:url value="/userform"/>"><i class="icon-settings"></i> Update your Information</a>
                            </li>
                                    <li class="external"><a href="logout">
                                    <i class="icon-power"></i> Logout
                                </a></li>
                            <!-- END USER LOGIN DROPDOWN -->
                           
                        </ul>    
</div>
                </div>
            </div>
            <!-- END HEADER TOP -->
       
        </div>
<div>
    <%@ include file="/common/messages.jsp" %>
    <div class="row">
        <decorator:body/>

        <c:if test="${currentMenu == 'AdminMenu'}">
            <div class="col-sm-2">
                <menu:useMenuDisplayer name="Velocity" config="navlistMenu.vm" permissions="rolesAdapter">
                    <menu:displayMenu name="AdminMenu"/>
                </menu:useMenuDisplayer>
            </div>
        </c:if>
    </div>
</div>
  <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container">
            <div class="row"><div class="col-sm-6" >
                <ul class="footer-left">
                <li><a href="#">About Us</a></li> 
                <li><a href="#">Terms of Service</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Contact Us</a></li>               
                </ul>
                </div>
                <div class="col-sm-6">
                <ul class="footer-right">
                <li><a href="#">Help</a></li> 
                <li><a href="#">How it Works</a></li>
                    <li><a href="#">Report a Problem</a></li>
                               
                </ul>
                </div></div>
                <div class="row">
                <div class="col-sm-12"><div class="copy">
                ESN &copy; 2016    
                </div></div>
                </div>
            </div>
        </div>
 <!-- BEGIN CORE PLUGINS -->
        <script src="<c:url value="assets/global/plugins/jquery.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/js.cookie.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery.blockui.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"/>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<c:url value="assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"/>" type="text/javascript"></script>
        <script src="<c:url value="assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"/>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<c:url value="assets/global/scripts/app.min.js"/>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
    
        <!-- END PAGE LEVEL SCRIPTS -->
       
</body>
</html>
